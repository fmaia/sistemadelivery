package br.edu.ifce.poo.delivery.teste;

import java.util.ArrayList;
import java.util.List;

import br.edu.ifce.poo.delivery.model.Cliente;
import br.edu.ifce.poo.delivery.model.Item;
import br.edu.ifce.poo.delivery.model.ItemPedido;
import br.edu.ifce.poo.delivery.model.Pedido;

public class Teste {
	public static void main(String[] args) {
		Item i1 = new Item(0, "Pizza", 22.00);
		Item i2 = new Item(1, "Refri 1L", 5.00);
		List<ItemPedido> lista =  
				new ArrayList<>(); 
		lista.add(
			new ItemPedido(i1, 1, "Calabreza"));
		lista.add(
			new ItemPedido(i2, 2, "Guarana"));
		Cliente c = new Cliente("Geomar",
					"Rua do sol, 15", "888888888");	
		Pedido p = new Pedido(c, lista, 
				"aguardando");
		System.out.println(p.calcValor());
	}
}
