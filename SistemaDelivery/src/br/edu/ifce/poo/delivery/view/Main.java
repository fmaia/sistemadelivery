package br.edu.ifce.poo.delivery.view;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.edu.ifce.poo.delivery.model.Cliente;
import br.edu.ifce.poo.delivery.model.Item;

public class Main {
	public static void main(String[] args) {
		List<Cliente> clientes = new ArrayList<>();
		List<Item> itens = new ArrayList<>();

		while(true) {
		  String token = 
		  JOptionPane.showInputDialog("Comando:");
		  switch(token) {
		  case "inserir cliente":
			  String nome = JOptionPane.
			  		showInputDialog("Nome:");
			  String telefone = JOptionPane.
				  	showInputDialog("Telefone:");
			  String endereco = JOptionPane.
					showInputDialog("Endereço:");
			  Cliente c = new Cliente(nome, 
					  	telefone, endereco);
			  clientes.add(c);  
			  break;
		  case "mostrar clientes":
			  String lista = "";
			  for(Cliente ci: clientes) {
				  lista += ci.getNome() + " - " +
					   ci.getEndereco() + " - " +
						 ci.getTelefone() + "\n";
			  }
			  JOptionPane.showMessageDialog
			  				(null, lista);
			  break;
		  case "inserir item":
			  int cod = Integer.parseInt(
				JOptionPane.showInputDialog("Código:")
				);
			  String desc = 
			    JOptionPane.
			    	showInputDialog("Descrição:");
			  double preco = Double.parseDouble(
				JOptionPane.showInputDialog("Preço")
			    );
			  Item item = new Item(cod, desc, preco);
			  itens.add(item);
			  break;
		  case "mostrar itens":
			  lista = "";
			  for(Item it: itens) {
				  lista += "["+it.getCodigo()+"]"+
						  it.getDescricao()+" -- "+
						  it.getPreco() + "\n";
			  }
			  JOptionPane.showMessageDialog(null, lista);
			  break;
		  case "sair":
			  return;
		  default:
			  break;
		  }
		}
	}

}
