package br.edu.ifce.poo.delivery.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import br.edu.ifce.poo.delivery.model.Cliente;

public class ClienteView {
	private JFrame janela;
	private JPanel painel;
	private JPanel painelForm;
	private JPanel painelTable;
	
	private JLabel rotuloNome;
	private JTextField campoNome;
	private JLabel rotuloEndereco;
	private JTextField campoEndereco;
	private JLabel rotuloTelefone;
	private JTextField campoTelefone;
	
	private JScrollPane tableSP;
	private JTable table;
	
	private JButton botaoSalvar;
	private JButton botaoDeletar;
	private JButton botaoAtualizar;
	
	public static List<Cliente> clientes
		= new ArrayList<>();
	
	public ClienteView() {
		painel = new JPanel();
		painelForm = new JPanel();
		janela = new JFrame("Cadastro Cliente");
		painel.setLayout(
		new BoxLayout(painel, BoxLayout.Y_AXIS));
		painelForm.setLayout(new GridLayout(0,2));
		
		rotuloNome = new JLabel("Nome:");
		painelForm.add(rotuloNome);
		campoNome = new JTextField(25);
		painelForm.add(campoNome);
		
		rotuloEndereco = new JLabel("Endereço:");
		painelForm.add(rotuloEndereco);
		campoEndereco = new JTextField(25);
		painelForm.add(campoEndereco);
		
		rotuloTelefone = new JLabel("Telefone:");
		painelForm.add(rotuloTelefone);
		campoTelefone = new JTextField(25);
		painelForm.add(campoTelefone);
		
		botaoSalvar = new JButton("Inserir");
		painelForm.add(botaoSalvar);
		botaoDeletar = new JButton("Deletar");
		painelForm.add(botaoDeletar);
		botaoAtualizar = new JButton("Atualizar");
		painelForm.add(botaoAtualizar);
		
		table = new JTable();
		painelTable = new JPanel();
		tableSP = new JScrollPane(table);
		painelTable.add(tableSP);
		
		painel.add(painelForm);
		painel.add(painelTable);
		janela.add(painel);
		loadTable();
		janela.setDefaultCloseOperation(
				JFrame.DISPOSE_ON_CLOSE);
		janela.pack();
		janela.setVisible(true);
	}
	public void run() {
		botaoSalvar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String nome = campoNome.getText();
				String end = campoEndereco.getText();
				String tel = campoTelefone.getText();
				Cliente c = new Cliente(nome, tel, end);
				clientes.add(c);
				loadTable();
			}
		});
		botaoDeletar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clientes.remove(table.getSelectedRow());
				loadTable();
				
			}
		});
		
		botaoAtualizar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int r = table.getSelectedRow();
				clientes.get(r).setNome((String)
						table.getModel().
						getValueAt(r, 0));
				clientes.get(r).setEndereco((String)
						table.getModel().
						getValueAt(r, 1));
				clientes.get(r).setTelefone((String)
						table.getModel().
						getValueAt(r, 2));
				loadTable();
			}
		});
	}
	private void loadTable() {
		String [][] dados = new String[clientes.size()][3];
		for(int i = 0; i < clientes.size();i++) {
		  dados[i][0] = clientes.get(i).getNome();
		  dados[i][1] = clientes.get(i).getEndereco();
		  dados[i][2] = clientes.get(i).getTelefone();
		}
		String [] colunas = {"Nome", "Endereço",
						"Telefone"};
		table.setModel(
		  new DefaultTableModel(dados, colunas));
		janela.revalidate();
	}
}







