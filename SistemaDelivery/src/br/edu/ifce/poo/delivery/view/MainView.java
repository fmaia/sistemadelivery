package br.edu.ifce.poo.delivery.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainView {
	private JFrame janela;
	private JPanel painel;
	
	private JButton botaoCliente;
	
	public MainView() {
		painel = new JPanel();
		janela = new JFrame("Sistema Delivery");
		painel.setLayout(new GridLayout(0,2));
		
		botaoCliente = new JButton
				("Cadastro Cliente");
		painel.add(botaoCliente);
		
		janela.add(painel);
		janela.setDefaultCloseOperation(
				JFrame.EXIT_ON_CLOSE);
		janela.pack();
		janela.setVisible(true);
	}
	
	public void run() {
		botaoCliente.addActionListener(
				new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ClienteView cv = new ClienteView();
				cv.run();
			}
		});
	}
	
	public static void main(String[] args) {
		MainView mv = new MainView();
		mv.run();
	}
}







